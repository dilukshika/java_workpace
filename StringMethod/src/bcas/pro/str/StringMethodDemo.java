package bcas.pro.str;

public class StringMethodDemo {
	public static void main(String[] args) {
		StringMethods demo = new StringMethods();
		
		String index = "I love my family very much ";
		String firstName = "Dilukshika";
		String lastName = "Sivanathan";
		String family = " The love of a family is life's greatest blessing";
		String name1 = "blessing";
		String name2 = "greatest";
		String name3 = "family";
		String name4 ="greatest";
		
		
	
		demo.charAt( index);
		demo.concat(firstName, lastName);
		demo.indexOf1(family);
		demo.indexOf2(family);
		demo.indexOf3(family,name1);
		demo.indexOf3(family, name2);
		demo.lastIndexOf1(family);
		demo.lastIndexOf2(family,name3);
		demo.lastIndexOf3(family,name3);
		demo.length(family, name4);
		demo.replace(family);
		demo.startsWith(family);
		demo.substring1(family);
		demo.substring2(family);
		demo.toLowerCase(family);
		demo.toUpperCase(family);
		demo.trim(family);
	}
}

// output 
/*
 
I love my family very much 
Test 1: String charAt() Method 
f
__________________________________________________
Dilukshika
Sivanathan
Test 5: String concat() Method 
Dilukshika Sivanathan
__________________________________________________
 The love of a family is life's greatest blessing
Test 16: String indexOf1(String family) Method 
Find Index = 3
__________________________________________________
 The love of a family is life's greatest blessing
Test 17: String indexOf2(String family) Method
Found Index = 3
__________________________________________________
 The love of a family is life's greatest blessing
blessing
Test 18: String indexOf3(String family, String name1) Method 
Found Index = 41
__________________________________________________
 The love of a family is life's greatest blessing
greatest
Test 18: String indexOf3(String family, String name1) Method 
Found Index = 32
__________________________________________________
 The love of a family is life's greatest blessing
Test 21: String lastIndexOf(int ch) Method
Found Last Index = 39
__________________________________________________
 The love of a family is life's greatest blessing
family
Test 23: String lastIndexOf1(String family) Method
Found Last Index = 15
__________________________________________________
 The love of a family is life's greatest blessing
family
Test 24: String lastIndexOf3(String family, String name3) Method
Found Last Index = 15
__________________________________________________
 The love of a family is life's greatest blessing
greatest
Test 25: String  length(String family, String name4) Method
String Length = 49
String Length = 8
__________________________________________________
 The love of a family is life's greatest blessing
Test 29: String replace(String family) Method
Return Value =  The love of T fTmily is life's greTtest blessing
Return Value =  The love of a famDly Ds lDfe's greatest blessDng
__________________________________________________
 The love of a family is life's greatest blessing
Test 34: String boolean startsWith(String family) Method
Return Value = false
Return Value = false
__________________________________________________
 The love of a family is life's greatest blessing
Test 37: String substring1(String family) Method
Return Value = y is life's greatest blessing
__________________________________________________
 The love of a family is life's greatest blessing
Test 38: String  substring2(String family) Method
Return Value =  family is life'
__________________________________________________
 The love of a family is life's greatest blessing
Test 40: String toLowerCase(String family) Method
Return Value =  the love of a family is life's greatest blessing
__________________________________________________
 The love of a family is life's greatest blessing
Test 43: String toUpperCase(String family) Method
Return Value =  THE LOVE OF A FAMILY IS LIFE'S GREATEST BLESSING
__________________________________________________
 The love of a family is life's greatest blessing
Test 45: String trim(String family) Method
Return Value =The love of a family is life's greatest blessing
__________________________________________________

*/