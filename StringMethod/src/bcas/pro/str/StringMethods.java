package bcas.pro.str;

public class StringMethods {
	
	public void charAt(String index) {
		char anwser = index.charAt(10);
		System.out.println(index);
		System.out.println("Test 1: String charAt() Method " );
		System.out.println( anwser);
		System.out.println("__________________________________________________");
	}

	public void concat(String firstName, String lastName) {
		String space = " ";
		String firstNameAndLastName = firstName.concat(space).concat(lastName);
		System.out.println(firstName);
		System.out.println(lastName);
		System.out.println("Test 5: String concat() Method ");
		System.out.println(firstNameAndLastName);
		System.out.println("__________________________________________________");
	}
	
	public void indexOf1(String family) {
		System.out.println(family);
		System.out.println("Test 16: String indexOf1(String family) Method " );
		System.out.println("Find Index = "+ family.indexOf("e"));
		System.out.println("__________________________________________________");
	}

	 
	public void indexOf2(String family) {
		System.out.println(family);
		System.out.println("Test 17: String indexOf2(String family) Method" );
		System.out.println("Found Index = "+ family.indexOf( 'e', 3 ));
		System.out.println("__________________________________________________");
	}

	
	public void indexOf3(String family, String name1) {
    	System.out.println(family);
    	System.out.println(name1);
    	System.out.println("Test 18: String indexOf3(String family, String name1) Method ");
    	System.out.println("Found Index = " +family.indexOf( name1 ));
    	System.out.println("__________________________________________________");
	}
	
	public void indexOf4(String family, String name2) {
    	System.out.println(family);
    	System.out.println(name2);
    	System.out.println("Test 19: String indexOf4(String family, String name2) Method" );
    	System.out.println( "Found Index = " + family.indexOf( name2, 50 ));
    	System.out.println("__________________________________________________");
	}

	public void lastIndexOf1(String family) {
    	System.out.println(family);
    	System.out.println("Test 21: String lastIndexOf(int ch) Method");
    	System.out.println("Found Last Index = " +family.lastIndexOf( 't' ));
    	System.out.println("__________________________________________________");
	}

	public void lastIndexOf2(String family, String name3) {
		System.out.println(family);
		System.out.println(name3);
		System.out.println("Test 23: String lastIndexOf1(String family) Method" );
		System.out.println( "Found Last Index = "+ family.lastIndexOf( name3 ));
		System.out.println("__________________________________________________");
	}
	
  
    public void lastIndexOf3(String family, String name3) {
    	System.out.println(family);
    	System.out.println(name3);
    	System.out.println("Test 24: String lastIndexOf3(String family, String name3) Method" );
    	System.out.println( "Found Last Index = " + family.lastIndexOf( name3, 20 ));
    	System.out.println("__________________________________________________");
    }
   
    public void length(String family, String name4) {
    	System.out.println(family);
    	System.out.println(name4);
    	System.out.println("Test 25: String  length(String family, String name4) Method");
    	System.out.println( "String Length = " + family.length());
    	System.out.println("String Length = " +  name4.length());
    	System.out.println("__________________________________________________");
    }
    
    public void replace(String family) { 
    	System.out.println(family);
    	System.out.println("Test 29: String replace(String family) Method" );
    	System.out.println("Return Value = " + family.replace('a', 'T'));
    	System.out.println("Return Value = " + family.replace('i', 'D'));
    	System.out.println("__________________________________________________");
    }
    
    public void startsWith(String family) {
    	System.out.println(family);
    	System.out.println("Test 34: String boolean startsWith(String family) Method" );
    	System.out.println("Return Value = " + family.startsWith("The"));
    	System.out.println("Return Value = " + family.startsWith("This"));
    	System.out.println("__________________________________________________");
    }
  
    public void substring1(String family) {
    	System.out.println(family);
    	System.out.println("Test 37: String substring1(String family) Method" );
    	System.out.println( "Return Value = " + family.substring(20));
    	System.out.println("__________________________________________________");
    }
    
    public void substring2(String family) {
    	System.out.println(family);
    	System.out.println("Test 38: String  substring2(String family) Method" );
    	System.out.println( "Return Value = " + family.substring(14, 30));
    	System.out.println("__________________________________________________");
    }
    
    
    public void toLowerCase(String family) {
    	System.out.println(family);
    	System.out.println("Test 40: String toLowerCase(String family) Method");
    	System.out.println("Return Value = " + family.toLowerCase());
    	System.out.println("__________________________________________________");
    }

    public void toUpperCase(String family) {
    	System.out.println(family);
    	System.out.println("Test 43: String toUpperCase(String family) Method");
    	System.out.println("Return Value = " +  family.toUpperCase() );
    	System.out.println("__________________________________________________");
    }
    
    
    public void trim(String family) {
    	System.out.println(family);
    	System.out.println("Test 45: String trim(String family) Method");
    	System.out.println("Return Value =" + family.trim());
    	System.out.println("__________________________________________________");
    }
    

}